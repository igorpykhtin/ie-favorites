# IE Favorites #

IE Favorites is an utility to manage Internet Explorer Favorites and about:Tabs page.

The latest binaries can be found on [Downloads](https://bitbucket.org/Cr7pto/ie-favorites/downloads) page.
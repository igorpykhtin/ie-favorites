﻿using System.Net;
using System.Threading;
using System.Threading.Tasks;

namespace IEFavorites.Presentation.Web
{
	public class WebHealthChecker
	{
		public async Task<WebHealthCheckResult> CheckHealthAsync(string url, CancellationToken cancellationToken)
		{
			bool result;
			string status;
			try
			{
				var httpStatus = await GetStatusAsync(url, cancellationToken);
				result = httpStatus == HttpStatusCode.OK;
				status = httpStatus.ToString();
			}
			catch (WebException ex)
			{
				result = false;
				status = ex.Status.ToString();
			}

			return new WebHealthCheckResult(result, status);
		}

		private async Task<HttpStatusCode> GetStatusAsync(string url, CancellationToken cancellationToken)
		{
			HttpStatusCode result = default(HttpStatusCode);

			try
			{
				var request = HttpWebRequest.Create(url);
				request.Method = "HEAD";
				using (var response = await request.GetResponseAsync(cancellationToken) as HttpWebResponse)
				{
					if (response != null)
					{
						result = response.StatusCode;
						response.Close();
					}
				}
			}
			catch (WebException ex)
			{
				var response = ex.Response as HttpWebResponse;
				if (response != null)
				{
					result = response.StatusCode;
				}
				else
				{
					throw;
				}
			}

			return result;
		}
	}
}

﻿using System.Net;

namespace IEFavorites.Presentation.Web
{
	public class WebHealthCheckResult
	{
		public static readonly WebHealthCheckResult Unknown = new WebHealthCheckResult(false, "Unknown");
		public static readonly WebHealthCheckResult Canceled = new WebHealthCheckResult(false, "Canceled");

		public bool IsActive { get; private set; }
		public string Status { get; private set; }
		public bool IsFaultStatus { get; private set; }

		public WebHealthCheckResult(bool isActive, string status)
		{
			this.IsActive = isActive;
			this.Status = status;
			this.IsFaultStatus = status == HttpStatusCode.NotFound.ToString()
				|| status == HttpStatusCode.InternalServerError.ToString()
				|| status == WebExceptionStatus.NameResolutionFailure.ToString()
				|| status == WebExceptionStatus.ConnectionClosed.ToString();
		}
	}
}

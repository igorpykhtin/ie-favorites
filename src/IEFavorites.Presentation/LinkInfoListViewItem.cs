﻿using System;
using System.Drawing;
using System.Windows.Forms;
using IEFavorites.Presentation.Web;

namespace IEFavorites.Presentation
{
	public class LinkInfoListViewItem : ListViewItem
	{
		public LinkInfo Link { get; }

		public LinkInfoListViewItem(LinkInfo link)
			: base(new ListViewSubItem[]
				{
					new ListViewSubItem { Name = "Name", Text = link.Name },
					new ListViewSubItem { Name = "Url", Text = link.Url },
					new ListViewSubItem { Name = "Status", Text = "Pending" },
				},
				imageIndex: -1
				)
		{
			Link = link;

			UpdateStatus();

			link.StatusChanged += linkInfo_StatusChanged;
		}

		void linkInfo_StatusChanged(object sender, EventArgs e)
		{
			UpdateStatus();
		}

		private void UpdateStatus()
		{
			SubItems["Status"].Text = Link.Status != null ? Link.Status.Status : "Pending";
			ForeColor = Link.Status != null
				? (Link.Status.IsActive
					? Color.Green
					: (Link.Status.IsFaultStatus
						? Color.Red
						: (Link.Status == WebHealthCheckResult.Unknown || Link.Status == WebHealthCheckResult.Canceled
							? Color.Gray
							: Color.Orange
							)))
				: Color.Gray;
		}
	}
}

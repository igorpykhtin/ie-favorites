﻿using System;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using IEFavorites.Presentation.Web;

namespace IEFavorites.Presentation
{
	public class FavoritesCheckerModel
	{
		private CancellationTokenSource _cancellation;

		public FavoritesCheckerModel()
		{
		}

		public string GetCurrentFolderPath()
		{
			return IEFavoritesRepository.GetCurrentFolderPath();
		}

		public LinkInfo[] GetLinks(string path)
		{
			//const int count = 100;
			return IEFavoritesRepository.GetLinkFiles(path)
				//.TakeWhile((filePath, index) => index < count)
				.Select(GetLinkInfo)
				.ToArray();
		}

		private LinkInfo GetLinkInfo(string filePath)
		{
			string linkName = Path.GetFileNameWithoutExtension(filePath);
			return new LinkInfo(filePath, linkName);
		}
		public async Task<ProcessStatus> ProcessLinksAsync(LinkInfo[] links, IProgress<LinkInfo> preparingProgress, IProgress<Tuple<LinkInfo, int>> processingProgress)
		{
			int processedCount = 0;

			using (_cancellation = new CancellationTokenSource())
			{
				var tasks = links.Select(link => ProcessLinkAsync(link, preparingProgress, _cancellation.Token)).ToList();

				while (tasks.Any() && !_cancellation.IsCancellationRequested)
				{
					var task = await Task.WhenAny(tasks);

					tasks.Remove(task);

					if (task.Status == TaskStatus.RanToCompletion)
					{
						var link = await task;

						int counter = Interlocked.Increment(ref processedCount);

						processingProgress.Report(Tuple.Create(link, counter));
					}
				}
			}

			_cancellation = null;

			return processedCount == links.Length ? ProcessStatus.Completed : ProcessStatus.Canceled;
		}

		private async Task<LinkInfo> ProcessLinkAsync(LinkInfo link, IProgress<LinkInfo> preparingProgress, CancellationToken cancellationToken)
		{
			await PrepareLinkAsync(link, cancellationToken);

			preparingProgress.Report(link);

			var linkStatus = await CheckLinkAsync(link, cancellationToken);

			link.UpdateStatus(linkStatus);

			cancellationToken.ThrowIfCancellationRequested();

			return link;
		}

		private async Task PrepareLinkAsync(LinkInfo link, CancellationToken cancellationToken)
		{
			link.Url = await IEFavoritesRepository.ExtractUrlAsync(link.FilePath, CancellationToken.None);
		}

		private async Task<WebHealthCheckResult> CheckLinkAsync(LinkInfo link, CancellationToken cancellationToken)
		{
			if (String.IsNullOrEmpty(link.Url))
			{
				return WebHealthCheckResult.Unknown;
			}

			if (cancellationToken.IsCancellationRequested)
			{
				return WebHealthCheckResult.Canceled;
			}

			var healthChecker = new WebHealthChecker();
			try
			{
				return await healthChecker.CheckHealthAsync(link.Url, cancellationToken);
			}
			catch (OperationCanceledException)
			{
				return WebHealthCheckResult.Canceled;
			}
		}

		public void CancelProcess()
		{
			if (_cancellation != null)
			{
				_cancellation.Cancel();
			}
		}

		public void DeleteLink(LinkInfo linkInfo)
		{
			IEFavoritesRepository.DeleteLink(linkInfo);
		}

		public void OpenInBrowser(LinkInfo linkInfo)
		{
			Process.Start(linkInfo.Url);
		}
	}

	public enum ProcessStatus
	{
		Completed,
		Canceled
	}
}

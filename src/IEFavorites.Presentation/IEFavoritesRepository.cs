﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace IEFavorites.Presentation
{
	public static class IEFavoritesRepository
	{
		public static string GetCurrentFolderPath()
		{
			return Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Favorites), "Links");
		}

		public static IEnumerable<string> GetLinkFiles(string path)
		{
			if (String.IsNullOrWhiteSpace(path))
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (!Directory.Exists(path))
			{
				throw new ArgumentException("Specified folder path not exist", nameof(path));
			}

			return Directory.GetFiles(path, "*.url", SearchOption.AllDirectories);
		}

		public static async Task<string> ExtractUrlAsync(string linkFilePath, CancellationToken ct)
		{
			const string prefix = "URL=";

			string content;
			using (var stream = new FileStream(linkFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite, 256, useAsync: true))
			using (var reader = new StreamReader(stream))
			{
				content = await reader.ReadToEndAsync();
			}

			string url = content != null
				? content.Split(new string[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries)
					.Where(line => line.StartsWith(prefix, StringComparison.CurrentCultureIgnoreCase))
					.Select(line => line.Substring(prefix.Length).Trim())
					.FirstOrDefault()
				: null;

			return url;
		}

		public static void DeleteLink(LinkInfo linkInfo)
		{
			File.Delete(linkInfo.FilePath);
		}
	}
}

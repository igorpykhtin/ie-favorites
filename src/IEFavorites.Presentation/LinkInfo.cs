﻿using System;
using IEFavorites.Presentation.Web;

namespace IEFavorites.Presentation
{
	public class LinkInfo
	{
		public string FilePath { get; private set; }
		public string Name { get; private set; }
		public string Url { get; set; }
		public WebHealthCheckResult Status { get; private set; }

		public event EventHandler StatusChanged;

		public LinkInfo(string filePath, string name)
		{
			this.FilePath = filePath;
			this.Name = name;
		}

		public void UpdateStatus(WebHealthCheckResult status)
		{
			this.Status = status;

			if (StatusChanged != null)
			{
				StatusChanged(this, EventArgs.Empty);
			}
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IEFavorites.Presentation
{
	public class FavoritesCheckerPresenter
	{
		private readonly IFavoritesCheckerView view;
		private readonly FavoritesCheckerModel model;

		public FavoritesCheckerPresenter(IFavoritesCheckerView view)
		{
			this.view = view;
			this.model = new FavoritesCheckerModel();
		}

		public void Initialize()
		{
			view.SetFolder(model.GetCurrentFolderPath());
			view.SetChangeFolderState(true);
			view.SetAnalyzeButtonState(true, false);
			view.SetRemoveLinkButtonState(false);
		}

		public void OnFolderChanged(string path)
		{
			view.SetFolder(path);
		}

		public async Task AnalyzeAsync()
		{
			string path = view.GetFolder();

			await AnalyzeInternalAsync(path);
		}

		private async Task AnalyzeInternalAsync(string path)
		{
			view.SetChangeFolderState(false);
			view.SetAnalyzeButtonState(false, false);

			view.SetProgressValue(0);
			view.SetProgressRange(0, 0);
			view.ClearItems();

			var links = await Task.Run<LinkInfo[]>(() => model.GetLinks(path));

			view.SetProgressRange(0, links.Length);
			view.SetAnalyzeButtonState(false, true);

			var status = await model.ProcessLinksAsync(
				links,
				new Progress<LinkInfo>(OnLinkPrepared),
				new Progress<Tuple<LinkInfo, int>>(OnLinkProcessed)
				);

			if (status == ProcessStatus.Canceled)
			{
				view.SetProgressValue(0);
			}

			view.SetChangeFolderState(true);
			view.SetAnalyzeButtonState(true, false);
		}

		public void CancelAnalyze()
		{
			view.SetAnalyzeButtonState(false, false);

			model.CancelProcess();
		}

		private void OnLinkPrepared(LinkInfo link)
		{
			view.AddItem(new LinkInfoListViewItem(link));
		}

		private void OnLinkProcessed(Tuple<LinkInfo, int> progress)
		{
			view.SetProgressValue(progress.Item2);
		}

		public void OnItemsChecked()
		{
			view.SetRemoveLinkButtonState(view.GetCheckedItems().Length > 0);
		}

		public void RemoveLinks()
		{
			if (view.IsRemoveConfirmed())
			{
				foreach (var item in view.GetCheckedItems())
				{
					model.DeleteLink(item.Link);

					view.RemoveItem(item);
				}
			}
		}

		public void OpenInBrowser()
		{
			var item = view.GetSelectedItems().FirstOrDefault();
			if (item == null)
			{
				return;
			}

			model.OpenInBrowser(item.Link);
		}

		public void ShowDuplicateItems()
		{
			var items = view.GetAllItems();

			var urls = new Dictionary<string, int>();

			foreach (var item in items.Where(o => !String.IsNullOrEmpty(o.Link.Url)))
			{
				int count;
				if (!urls.TryGetValue(item.Link.Url, out count))
				{
					count = 0;
				}
				urls[item.Link.Url] = count + 1;
			}

			var visibleItems = items.Where(o => !String.IsNullOrEmpty(o.Link.Url) && urls[o.Link.Url] > 1).ToArray();

			view.ClearItems();
			view.AddItems(visibleItems);
		}
	}
}

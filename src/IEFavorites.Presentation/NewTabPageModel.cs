﻿using System;
using Microsoft.Win32;

namespace IEFavorites.Presentation
{
	public class NewTabPageModel
	{
		const string NewTabPageKey = @"Software\Microsoft\Internet Explorer\TabbedBrowsing\NewTabPage";
		const string NewTabPageExcludedKey = NewTabPageKey + @"\Exclude";
		const string NumRowsValueName = "NumRows";
		public readonly int MinNumberOfRows = 2;
		public readonly int MaxNumberOfRows = 5;
		const int DefaultNumberOfRows = 2;

		/// <remarks>HKCU\Software\Microsoft\Internet Explorer\TabbedBrowsing\NewTabPage\NumRows (DWORD)</remarks>
		public int GetNumberOfRows()
		{
			using (RegistryKey key = Registry.CurrentUser.OpenSubKey(NewTabPageKey, true))
			{
				return Convert.ToInt32(key.GetValue(NumRowsValueName, DefaultNumberOfRows));
			}
		}

		/// <remarks>HKCU\Software\Microsoft\Internet Explorer\TabbedBrowsing\NewTabPage\NumRows (DWORD)</remarks>
		public void SetNumberOfRows(int rows)
		{
			using (RegistryKey key = Registry.CurrentUser.OpenSubKey(NewTabPageKey, true))
			{
				key.SetValue(NumRowsValueName, rows.ToString(), RegistryValueKind.DWord);
			}
		}

		/// <remarks>HKCU\Software\Microsoft\Internet Explorer\TabbedBrowsing\NewTabPage\Excluded</remarks>
		public void ResetFrequentList()
		{
			// remove all entries under 'Excluded' key
			using (RegistryKey key = Registry.CurrentUser.OpenSubKey(NewTabPageExcludedKey, true))
			{
				if (key == null)
				{
					return;
				}

				foreach (var valueToExclude in key.GetValueNames())
				{
					key.DeleteValue(valueToExclude);
				}
			}
		}
	}
}

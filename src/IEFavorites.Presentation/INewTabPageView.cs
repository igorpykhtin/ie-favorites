﻿
namespace IEFavorites.Presentation
{
	public interface INewTabPageView
	{
		int GetNumberOfRows();
		void SetNumberOfRows(int value, int minValue, int maxValue);
		void SetUpdateNumberOfRowsEnabled(bool enabled);
		bool ConfirmResetFrequentList();
		void ShowResetFrequentListCompletedMessage();
	}
}

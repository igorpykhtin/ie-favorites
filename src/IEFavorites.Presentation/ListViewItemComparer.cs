﻿using System;
using System.Collections;
using System.Windows.Forms;

namespace IEFavorites.Presentation
{
	public class ListViewItemComparer : IComparer
	{
		public int Column { get; }
		public SortOrder Order { get; }

		public ListViewItemComparer()
			: this(0, SortOrder.Ascending)
		{
		}

		public ListViewItemComparer(int column, SortOrder order)
		{
			this.Column = column;
			this.Order = order;
		}

		public int Compare(object x, object y)
		{
			int result = -1;
			result = String.Compare(((ListViewItem)x).SubItems[Column].Text, ((ListViewItem)y).SubItems[Column].Text);

			if (Order == SortOrder.Descending)
			{
				result *= -1;
			}
			return result;
		}
	}
}

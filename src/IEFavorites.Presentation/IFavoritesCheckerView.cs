﻿using System.Collections.Generic;
using System.Windows.Forms;

namespace IEFavorites.Presentation
{
	public interface IFavoritesCheckerView
	{
		string GetFolder();
		void SetFolder(string path);
		void SetRemoveLinkButtonState(bool enabled);
		void SetAnalyzeButtonState(bool enabled, bool cancelEnabled);
		void SetChangeFolderState(bool enabled);
		void SetProgressRange(int minimum, int maximum);
		void SetProgressValue(int value);
		LinkInfoListViewItem[] GetAllItems();
		LinkInfoListViewItem[] GetCheckedItems();
		LinkInfoListViewItem[] GetSelectedItems();
		bool IsRemoveConfirmed();
		void ClearItems();
		void AddItem(ListViewItem item);
		void AddItems(IEnumerable<ListViewItem> items);
		void RemoveItem(ListViewItem item);
	}
}

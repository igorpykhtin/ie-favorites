﻿
namespace IEFavorites.Presentation
{
	public class NewTabPagePresenter
	{
		private readonly INewTabPageView _view;
		private readonly NewTabPageModel _model;

		public NewTabPagePresenter(INewTabPageView view)
		{
			_view = view;
			_model = new NewTabPageModel();
		}

		public void BindData()
		{
			int minRows = _model.MinNumberOfRows;
			int maxRows = _model.MaxNumberOfRows;
			int rows = _model.GetNumberOfRows();

			_view.SetNumberOfRows(rows, minRows, maxRows);
		}

		public void OnNumberOfRowsChanged()
		{
			int newRows = _view.GetNumberOfRows();
			int currentRows = _model.GetNumberOfRows();

			_view.SetUpdateNumberOfRowsEnabled(newRows != currentRows);
		}

		public void UpdateNumberOfRows()
		{
			int rows = _view.GetNumberOfRows();

			_model.SetNumberOfRows(rows);

			_view.SetUpdateNumberOfRowsEnabled(false);
		}

		public void ResetFrequentList()
		{
			if (!_view.ConfirmResetFrequentList())
			{
				return;
			}

			_model.ResetFrequentList();

			_view.ShowResetFrequentListCompletedMessage();
		}
	}
}

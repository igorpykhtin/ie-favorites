﻿namespace IEFavorites.Forms
{
	partial class IEFavoritesControl
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.lblFolder = new System.Windows.Forms.Label();
			this.tbFolder = new System.Windows.Forms.TextBox();
			this.btnChangeFolder = new System.Windows.Forms.Button();
			this.btnAnalyze = new System.Windows.Forms.Button();
			this.dlgFolder = new System.Windows.Forms.FolderBrowserDialog();
			this.btnRemoveLink = new System.Windows.Forms.Button();
			this.progressBar = new System.Windows.Forms.ProgressBar();
			this.viewLinks = new System.Windows.Forms.ListView();
			this.NameColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.UrlColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.StatusColumn = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
			this.btnCancelAnalyze = new System.Windows.Forms.Button();
			this.btnShowDuplicateItems = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// lblFolder
			// 
			this.lblFolder.AutoSize = true;
			this.lblFolder.Location = new System.Drawing.Point(-3, 7);
			this.lblFolder.Name = "lblFolder";
			this.lblFolder.Size = new System.Drawing.Size(85, 13);
			this.lblFolder.TabIndex = 0;
			this.lblFolder.Text = "Favorites Folder:";
			// 
			// tbFolder
			// 
			this.tbFolder.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.tbFolder.Location = new System.Drawing.Point(88, 4);
			this.tbFolder.Name = "tbFolder";
			this.tbFolder.ReadOnly = true;
			this.tbFolder.Size = new System.Drawing.Size(523, 20);
			this.tbFolder.TabIndex = 1;
			// 
			// btnChangeFolder
			// 
			this.btnChangeFolder.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnChangeFolder.Location = new System.Drawing.Point(617, 2);
			this.btnChangeFolder.Name = "btnChangeFolder";
			this.btnChangeFolder.Size = new System.Drawing.Size(75, 23);
			this.btnChangeFolder.TabIndex = 2;
			this.btnChangeFolder.Text = "Browse...";
			this.btnChangeFolder.UseVisualStyleBackColor = true;
			this.btnChangeFolder.Click += new System.EventHandler(this.btnChangeFolder_Click);
			// 
			// btnAnalyze
			// 
			this.btnAnalyze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnAnalyze.Location = new System.Drawing.Point(451, 46);
			this.btnAnalyze.Name = "btnAnalyze";
			this.btnAnalyze.Size = new System.Drawing.Size(160, 23);
			this.btnAnalyze.TabIndex = 3;
			this.btnAnalyze.Text = "Analyze";
			this.btnAnalyze.UseVisualStyleBackColor = true;
			this.btnAnalyze.Click += new System.EventHandler(this.btnAnalyze_Click);
			// 
			// btnRemoveLink
			// 
			this.btnRemoveLink.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnRemoveLink.Location = new System.Drawing.Point(617, 107);
			this.btnRemoveLink.Name = "btnRemoveLink";
			this.btnRemoveLink.Size = new System.Drawing.Size(75, 23);
			this.btnRemoveLink.TabIndex = 6;
			this.btnRemoveLink.Text = "Remove";
			this.btnRemoveLink.UseVisualStyleBackColor = true;
			this.btnRemoveLink.Click += new System.EventHandler(this.btnRemoveLink_Click);
			// 
			// progressBar
			// 
			this.progressBar.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.progressBar.Location = new System.Drawing.Point(0, 75);
			this.progressBar.Name = "progressBar";
			this.progressBar.Size = new System.Drawing.Size(692, 23);
			this.progressBar.TabIndex = 7;
			// 
			// viewLinks
			// 
			this.viewLinks.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.viewLinks.CheckBoxes = true;
			this.viewLinks.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.NameColumn,
            this.UrlColumn,
            this.StatusColumn});
			this.viewLinks.FullRowSelect = true;
			this.viewLinks.GridLines = true;
			this.viewLinks.Location = new System.Drawing.Point(0, 107);
			this.viewLinks.Name = "viewLinks";
			this.viewLinks.Size = new System.Drawing.Size(608, 389);
			this.viewLinks.TabIndex = 5;
			this.viewLinks.UseCompatibleStateImageBehavior = false;
			this.viewLinks.View = System.Windows.Forms.View.Details;
			this.viewLinks.ColumnClick += new System.Windows.Forms.ColumnClickEventHandler(this.viewLinks_ColumnClick);
			this.viewLinks.ItemChecked += new System.Windows.Forms.ItemCheckedEventHandler(this.viewLinks_ItemChecked);
			this.viewLinks.DoubleClick += new System.EventHandler(this.viewLinks_DoubleClick);
			// 
			// NameColumn
			// 
			this.NameColumn.Text = "Title";
			this.NameColumn.Width = 300;
			// 
			// UrlColumn
			// 
			this.UrlColumn.Text = "URL";
			this.UrlColumn.Width = 200;
			// 
			// StatusColumn
			// 
			this.StatusColumn.Text = "Status";
			// 
			// btnCancelAnalyze
			// 
			this.btnCancelAnalyze.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnCancelAnalyze.Location = new System.Drawing.Point(617, 46);
			this.btnCancelAnalyze.Name = "btnCancelAnalyze";
			this.btnCancelAnalyze.Size = new System.Drawing.Size(75, 23);
			this.btnCancelAnalyze.TabIndex = 4;
			this.btnCancelAnalyze.Text = "Cancel";
			this.btnCancelAnalyze.UseVisualStyleBackColor = true;
			this.btnCancelAnalyze.Click += new System.EventHandler(this.btnCancelAnalyze_Click);
			// 
			// btnShowDuplicateItems
			// 
			this.btnShowDuplicateItems.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.btnShowDuplicateItems.Location = new System.Drawing.Point(285, 46);
			this.btnShowDuplicateItems.Name = "btnShowDuplicateItems";
			this.btnShowDuplicateItems.Size = new System.Drawing.Size(160, 23);
			this.btnShowDuplicateItems.TabIndex = 8;
			this.btnShowDuplicateItems.Text = "Show Duplicate Items";
			this.btnShowDuplicateItems.UseVisualStyleBackColor = true;
			this.btnShowDuplicateItems.Click += new System.EventHandler(this.btnShowDuplicateItems_Click);
			// 
			// IEFavoritesControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnShowDuplicateItems);
			this.Controls.Add(this.btnCancelAnalyze);
			this.Controls.Add(this.viewLinks);
			this.Controls.Add(this.progressBar);
			this.Controls.Add(this.btnRemoveLink);
			this.Controls.Add(this.btnAnalyze);
			this.Controls.Add(this.btnChangeFolder);
			this.Controls.Add(this.tbFolder);
			this.Controls.Add(this.lblFolder);
			this.Name = "IEFavoritesControl";
			this.Size = new System.Drawing.Size(692, 500);
			this.Load += new System.EventHandler(this.MainForm_Load);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label lblFolder;
		private System.Windows.Forms.TextBox tbFolder;
		private System.Windows.Forms.Button btnChangeFolder;
		private System.Windows.Forms.Button btnAnalyze;
		private System.Windows.Forms.FolderBrowserDialog dlgFolder;
		private System.Windows.Forms.Button btnRemoveLink;
		private System.Windows.Forms.ProgressBar progressBar;
		private System.Windows.Forms.ListView viewLinks;
		private System.Windows.Forms.ColumnHeader NameColumn;
		private System.Windows.Forms.ColumnHeader UrlColumn;
		private System.Windows.Forms.ColumnHeader StatusColumn;
		private System.Windows.Forms.Button btnCancelAnalyze;
		private System.Windows.Forms.Button btnShowDuplicateItems;
	}
}


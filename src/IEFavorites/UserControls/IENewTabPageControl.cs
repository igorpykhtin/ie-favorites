﻿using System;
using System.Windows.Forms;
using IEFavorites.Presentation;

namespace IEFavorites.UserControls
{
	public partial class IENewTabPageControl : UserControl, INewTabPageView
	{
		private NewTabPagePresenter _presenter;

		public IENewTabPageControl()
		{
			InitializeComponent();

			_presenter = new NewTabPagePresenter(this);
		}

		private void IENewTabPageControl_Load(object sender, EventArgs e)
		{
			_presenter.BindData();
		}

		private void numRows_ValueChanged(object sender, EventArgs e)
		{
			_presenter.OnNumberOfRowsChanged();
		}

		private void btnUpdateRows_Click(object sender, EventArgs e)
		{
			_presenter.UpdateNumberOfRows();
		}

		private void btnResetFrequentList_Click(object sender, EventArgs e)
		{
			_presenter.ResetFrequentList();
		}

		int INewTabPageView.GetNumberOfRows()
		{
			return Convert.ToInt32(numRows.Value);
		}

		void INewTabPageView.SetNumberOfRows(int value, int minValue, int maxValue)
		{
			numRows.Minimum = minValue;
			numRows.Maximum = maxValue;
			numRows.Value = value;
		}

		void INewTabPageView.SetUpdateNumberOfRowsEnabled(bool enabled)
		{
			btnUpdateRows.Enabled = enabled;
		}

		bool INewTabPageView.ConfirmResetFrequentList()
		{
			return MessageBox.Show(
				this,
				"Are you sure you want to reset \"Frequent\" list in Internet Explorer?",
				ParentForm.Text,
				MessageBoxButtons.YesNo,
				MessageBoxIcon.Warning
				) == DialogResult.Yes;
		}

		void INewTabPageView.ShowResetFrequentListCompletedMessage()
		{
			MessageBox.Show(
				this,
				"\"Frequent\" list in Internet Explorer successfully reset." + Environment.NewLine
					+ Environment.NewLine
					+ "Note: To apply changes, press F5 in the New Tab page in Internet Explorer.",
				ParentForm.Text,
				MessageBoxButtons.OK,
				MessageBoxIcon.Information
				);
		}
	}
}

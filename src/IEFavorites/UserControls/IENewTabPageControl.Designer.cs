﻿namespace IEFavorites.UserControls
{
	partial class IENewTabPageControl
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.numRows = new System.Windows.Forms.NumericUpDown();
			this.btnResetFrequentList = new System.Windows.Forms.Button();
			this.btnUpdateRows = new System.Windows.Forms.Button();
			((System.ComponentModel.ISupportInitialize)(this.numRows)).BeginInit();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(-3, 7);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(89, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Number of Rows:";
			// 
			// numRows
			// 
			this.numRows.Location = new System.Drawing.Point(92, 4);
			this.numRows.Name = "numRows";
			this.numRows.Size = new System.Drawing.Size(72, 20);
			this.numRows.TabIndex = 1;
			this.numRows.ValueChanged += new System.EventHandler(this.numRows_ValueChanged);
			// 
			// btnResetFrequentList
			// 
			this.btnResetFrequentList.Location = new System.Drawing.Point(0, 75);
			this.btnResetFrequentList.Name = "btnResetFrequentList";
			this.btnResetFrequentList.Size = new System.Drawing.Size(164, 23);
			this.btnResetFrequentList.TabIndex = 3;
			this.btnResetFrequentList.Text = "Reset \"Frequent\" List";
			this.btnResetFrequentList.UseVisualStyleBackColor = true;
			this.btnResetFrequentList.Click += new System.EventHandler(this.btnResetFrequentList_Click);
			// 
			// btnUpdateRows
			// 
			this.btnUpdateRows.Location = new System.Drawing.Point(170, 2);
			this.btnUpdateRows.Name = "btnUpdateRows";
			this.btnUpdateRows.Size = new System.Drawing.Size(75, 23);
			this.btnUpdateRows.TabIndex = 2;
			this.btnUpdateRows.Text = "Update";
			this.btnUpdateRows.UseVisualStyleBackColor = true;
			this.btnUpdateRows.Click += new System.EventHandler(this.btnUpdateRows_Click);
			// 
			// IENewTabPageControl
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.Controls.Add(this.btnUpdateRows);
			this.Controls.Add(this.btnResetFrequentList);
			this.Controls.Add(this.numRows);
			this.Controls.Add(this.label1);
			this.Name = "IENewTabPageControl";
			this.Size = new System.Drawing.Size(439, 308);
			this.Load += new System.EventHandler(this.IENewTabPageControl_Load);
			((System.ComponentModel.ISupportInitialize)(this.numRows)).EndInit();
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.NumericUpDown numRows;
		private System.Windows.Forms.Button btnResetFrequentList;
		private System.Windows.Forms.Button btnUpdateRows;
	}
}

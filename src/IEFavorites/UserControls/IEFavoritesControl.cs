﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using IEFavorites.Presentation;

namespace IEFavorites.Forms
{
	public partial class IEFavoritesControl : UserControl, IFavoritesCheckerView
	{
		private readonly FavoritesCheckerPresenter presenter;

		public IEFavoritesControl()
		{
			InitializeComponent();

			viewLinks.DoubleBuffered(true);

			presenter = new FavoritesCheckerPresenter(this);
		}

		private void MainForm_Load(object sender, EventArgs e)
		{
			presenter.Initialize();
		}

		private void btnChangeFolder_Click(object sender, EventArgs e)
		{
			if (dlgFolder.ShowDialog(this) == DialogResult.OK)
			{
				presenter.OnFolderChanged(dlgFolder.SelectedPath);
			}
		}

		private async void btnAnalyze_Click(object sender, EventArgs e)
		{
			await presenter.AnalyzeAsync();
		}

		private void btnCancelAnalyze_Click(object sender, EventArgs e)
		{
			presenter.CancelAnalyze();
		}

		private void btnShowDuplicateItems_Click(object sender, EventArgs e)
		{
			presenter.ShowDuplicateItems();
		}

		private void viewLinks_ItemChecked(object sender, ItemCheckedEventArgs e)
		{
			presenter.OnItemsChecked();
		}

		private void viewLinks_ColumnClick(object sender, ColumnClickEventArgs e)
		{
			int column;
			SortOrder order;

			var currentComparer = viewLinks.ListViewItemSorter as ListViewItemComparer;
			if (currentComparer != null && currentComparer.Column == e.Column)
			{
				column = e.Column;
				order = currentComparer.Order == SortOrder.Ascending ? SortOrder.Descending : SortOrder.Ascending;
			}
			else
			{
				column = e.Column;
				order = SortOrder.Ascending;
			}

			viewLinks.ListViewItemSorter = new ListViewItemComparer(column, order);
			viewLinks.Sort();
		}

		private void btnRemoveLink_Click(object sender, EventArgs e)
		{
			presenter.RemoveLinks();
		}

		private void viewLinks_DoubleClick(object sender, EventArgs e)
		{
			presenter.OpenInBrowser();
		}

		string IFavoritesCheckerView.GetFolder()
		{
			return tbFolder.Text;
		}

		LinkInfoListViewItem[] IFavoritesCheckerView.GetAllItems()
		{
			return viewLinks.Items.Cast<LinkInfoListViewItem>().ToArray();
		}

		LinkInfoListViewItem[] IFavoritesCheckerView.GetCheckedItems()
		{
			return viewLinks.CheckedItems.Cast<LinkInfoListViewItem>().ToArray();
		}

		LinkInfoListViewItem[] IFavoritesCheckerView.GetSelectedItems()
		{
			return viewLinks.SelectedItems.Cast<LinkInfoListViewItem>().ToArray();
		}

		void IFavoritesCheckerView.SetFolder(string path)
		{
			tbFolder.Text = path;
			dlgFolder.SelectedPath = path;
		}

		void IFavoritesCheckerView.SetRemoveLinkButtonState(bool enabled)
		{
			btnRemoveLink.Enabled = enabled;
		}

		void IFavoritesCheckerView.SetAnalyzeButtonState(bool enabled, bool cancelEnabled)
		{
			btnAnalyze.Enabled = enabled;
			btnCancelAnalyze.Enabled = cancelEnabled;
			btnShowDuplicateItems.Enabled = enabled && viewLinks.Items.Count > 0;
		}

		void IFavoritesCheckerView.SetChangeFolderState(bool enabled)
		{
			btnChangeFolder.Enabled = enabled;
		}

		void IFavoritesCheckerView.SetProgressRange(int minimum, int maximum)
		{
			progressBar.Minimum = minimum;
			progressBar.Maximum = maximum;
		}

		void IFavoritesCheckerView.SetProgressValue(int value)
		{
			progressBar.Value = value;
		}

		bool IFavoritesCheckerView.IsRemoveConfirmed()
		{
			return MessageBox.Show(
				this,
				"Are you sure to remove selected links?",
				"Remove Favorite Links",
				MessageBoxButtons.YesNo,
				MessageBoxIcon.Question
				) == DialogResult.Yes;
		}

		void IFavoritesCheckerView.ClearItems()
		{
			viewLinks.Items.Clear();
		}

		void IFavoritesCheckerView.AddItem(ListViewItem item)
		{
			viewLinks.Items.Add(item);
		}

		void IFavoritesCheckerView.AddItems(IEnumerable<ListViewItem> items)
		{
			viewLinks.Items.AddRange(items.ToArray());
		}

		void IFavoritesCheckerView.RemoveItem(ListViewItem item)
		{
			item.Checked = false;
			item.Remove();
		}
	}
}

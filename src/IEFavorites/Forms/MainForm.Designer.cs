﻿namespace IEFavorites.Forms
{
	partial class MainForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.tabControl = new System.Windows.Forms.TabControl();
			this.tabIEFavorites = new System.Windows.Forms.TabPage();
			this.ctlIEFavorites = new IEFavorites.Forms.IEFavoritesControl();
			this.tabIENewTabPage = new System.Windows.Forms.TabPage();
			this.ctlIENewTabPage = new IEFavorites.UserControls.IENewTabPageControl();
			this.tabControl.SuspendLayout();
			this.tabIEFavorites.SuspendLayout();
			this.tabIENewTabPage.SuspendLayout();
			this.SuspendLayout();
			// 
			// tabControl
			// 
			this.tabControl.Controls.Add(this.tabIEFavorites);
			this.tabControl.Controls.Add(this.tabIENewTabPage);
			this.tabControl.Dock = System.Windows.Forms.DockStyle.Fill;
			this.tabControl.Location = new System.Drawing.Point(8, 8);
			this.tabControl.Name = "tabControl";
			this.tabControl.SelectedIndex = 0;
			this.tabControl.Size = new System.Drawing.Size(719, 514);
			this.tabControl.TabIndex = 1;
			// 
			// tabIEFavorites
			// 
			this.tabIEFavorites.Controls.Add(this.ctlIEFavorites);
			this.tabIEFavorites.Location = new System.Drawing.Point(4, 22);
			this.tabIEFavorites.Name = "tabIEFavorites";
			this.tabIEFavorites.Padding = new System.Windows.Forms.Padding(8);
			this.tabIEFavorites.Size = new System.Drawing.Size(711, 488);
			this.tabIEFavorites.TabIndex = 0;
			this.tabIEFavorites.Text = "Favorites";
			this.tabIEFavorites.UseVisualStyleBackColor = true;
			// 
			// ctlIEFavorites
			// 
			this.ctlIEFavorites.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctlIEFavorites.Location = new System.Drawing.Point(8, 8);
			this.ctlIEFavorites.Name = "ctlIEFavorites";
			this.ctlIEFavorites.Size = new System.Drawing.Size(695, 472);
			this.ctlIEFavorites.TabIndex = 1;
			// 
			// tabIENewTabPage
			// 
			this.tabIENewTabPage.Controls.Add(this.ctlIENewTabPage);
			this.tabIENewTabPage.Location = new System.Drawing.Point(4, 22);
			this.tabIENewTabPage.Name = "tabIENewTabPage";
			this.tabIENewTabPage.Padding = new System.Windows.Forms.Padding(8);
			this.tabIENewTabPage.Size = new System.Drawing.Size(711, 488);
			this.tabIENewTabPage.TabIndex = 1;
			this.tabIENewTabPage.Text = "New Tab Page";
			this.tabIENewTabPage.UseVisualStyleBackColor = true;
			// 
			// ctlIENewTabPage
			// 
			this.ctlIENewTabPage.Dock = System.Windows.Forms.DockStyle.Fill;
			this.ctlIENewTabPage.Location = new System.Drawing.Point(8, 8);
			this.ctlIENewTabPage.Name = "ctlIENewTabPage";
			this.ctlIENewTabPage.Size = new System.Drawing.Size(695, 472);
			this.ctlIENewTabPage.TabIndex = 0;
			// 
			// MainForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(735, 530);
			this.Controls.Add(this.tabControl);
			this.MinimumSize = new System.Drawing.Size(700, 300);
			this.Name = "MainForm";
			this.Padding = new System.Windows.Forms.Padding(8);
			this.SizeGripStyle = System.Windows.Forms.SizeGripStyle.Show;
			this.Text = "IE Favorites";
			this.tabControl.ResumeLayout(false);
			this.tabIEFavorites.ResumeLayout(false);
			this.tabIENewTabPage.ResumeLayout(false);
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.TabControl tabControl;
		private System.Windows.Forms.TabPage tabIEFavorites;
		private System.Windows.Forms.TabPage tabIENewTabPage;
		private IEFavoritesControl ctlIEFavorites;
		private UserControls.IENewTabPageControl ctlIENewTabPage;

	}
}

